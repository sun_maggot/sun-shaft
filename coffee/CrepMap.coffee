pixi = require 'pixi.js'
Entity = require './Entity.coffee'

radialBlurFragSrc = '''
precision mediump float;

varying vec2 vTextureCoord;
varying vec4 vColor;

uniform vec2 position;
uniform sampler2D uSampler;
uniform float rayLength;

#define SAMPLE_DENSITY 16
#define SAMPLE_COUNT 1000
#define PI 3.1415926

void main(void)
{
    float pixelXSize = 1.0 / 240.0;
    float pixelYSize = 1.0 / 200.0;

    vec2 dirVector = vTextureCoord - position;  // Unnormalized direction
    float dist = length(dirVector);
    vec2 dir = normalize(dirVector);  // Normalize direction
    vec2 pixelDir = dir * vec2(pixelXSize, pixelYSize);
    float sampleLength = rayLength * length(pixelDir) / float(SAMPLE_COUNT);

    vec4 color = texture2D(uSampler, vTextureCoord); 
    vec4 sum = color;

	for (int i = 1; i < SAMPLE_COUNT; i++)
	{
		vec2 offset = float(i) * dir * sampleLength;
		if (length(offset) > dist)
		{
			break;
		}
		vec2 sampleCoord = vTextureCoord - offset;
		float ratio = float(i) / float(SAMPLE_COUNT);
		float falloff = cos(ratio * PI) / 2.0 + 0.5;

		vec4 sample = texture2D(uSampler, sampleCoord) * falloff;
		color.r = max(color.r, sample.r);
		color.g = max(color.g, sample.g);
		color.b = max(color.b, sample.b);
		sum += sample;
	}

	sum /= float(SAMPLE_COUNT) * 0.2;
    gl_FragColor = 0.5 * (sum + color);
}
'''

class RadialBlurFilter extends pixi.AbstractFilter
	constructor: () ->
		uni =
			position:
				type: '2f'
				value: [ 0.5, 0.5 ]
			rayLength:
				type: '1f'
				value: [ 150 ]
		super null, radialBlurFragSrc, uni

Object.defineProperty RadialBlurFilter.prototype, 'x',
	get: () -> @uniforms.position.value[0]
	set: (val) -> @uniforms.position.value[0] = val

Object.defineProperty RadialBlurFilter.prototype, 'y',
	get: () -> @uniforms.position.value[1]
	set: (val) -> @uniforms.position.value[1] = val

module.exports = class CrepMap extends Entity
	@TYPE('CrepMap', this)
	constructor: (game) ->
		super(	game)
		@parallax.x = 0
		@parallax.y = 0
		@z = 3

		@radialBlur = new RadialBlurFilter
		@colorStep = new pixi.filters.ColorStepFilter
		@colorStep.step = 15

		@clearGraphics = new pixi.Graphics
		@clearGraphics.beginFill 0x000000, 1
		@clearGraphics.drawRect 0, 0, @game.width, @game.height
		@clearGraphics.endFill()

		@midTexture = new pixi.RenderTexture(@game.renderer, @game.width, @game.height, pixi.SCALE_MODES.NEAREST)
		@midSprite = new pixi.Sprite @midTexture
		@midSprite.tint = 0x000000
		@midContainer = new pixi.Container
		@midContainer.addChild @midSprite
		# @displayObject.addChild @midContainer

		@mainTexture = new pixi.RenderTexture(@game.renderer, @game.width, @game.height, pixi.SCALE_MODES.NEAREST)
		@mainSprite = new pixi.Sprite @mainTexture
		@mainContainer = new pixi.Container
		@mainContainer.addChild @mainSprite
		@mainSprite.filters = [ @radialBlur ]
		# @displayObject.addChild @mainContainer

		@finalTexture = new pixi.RenderTexture(@game.renderer, @game.width, @game.height, pixi.SCALE_MODES.NEAREST)
		@finalSprite = new pixi.Sprite @finalTexture
		@finalSprite.blendMode = pixi.BLEND_MODES.ADD
		@displayObject.addChild @finalSprite

		@source = null
		@blockers = []

		@tempMatrix = new pixi.Matrix()

	draw: (dt) ->
		super(dt)

		if not @source?
			return

		@scene.root.updateTransform()

		@midTexture.clear()
		@mainTexture.clear()
		@finalTexture.clear()
		@mainTexture.render @clearGraphics
		@finalTexture.render @clearGraphics
		@mainTexture.render @source.light, @source.displayObject.worldTransform.copy(@tempMatrix)

		globalPos = @source.displayObject.toGlobal({x: 0, y: 0})
		globalPos.x = Math.floor(globalPos.x)
		globalPos.y = Math.floor(globalPos.y)
		globalPos.x /= @game.width
		globalPos.y /= @game.height
		@radialBlur.x = globalPos.x
		@radialBlur.y = globalPos.y

		for blocker in @blockers
			d = blocker.displayObject
			@midTexture.render d, d.worldTransform.copy(@tempMatrix)

		@mainTexture.render @midSprite
		@finalTexture.render @mainSprite
