pixi = require 'pixi.js'
imageManifest = require '../images/manifest.json'
q = require 'q'
$ = require 'jquery'

loader = new pixi.loaders.Loader()
imagesLoaded = q.defer()
for image in imageManifest
	loader.add image, image
loader.once 'complete', (loader) ->
	baseTextures = {}
	for name, res of loader.resources
		baseTextures[name] = res.texture.baseTexture
		baseTextures[name].scaleMode = pixi.SCALE_MODES.NEAREST
	imagesLoaded.resolve baseTextures

loader.load()

finished = q.defer()
q.all imagesLoaded.promise
.then (baseTextures) ->
	finished.resolve
		baseTextures: baseTextures


module.exports =
	finished: finished.promise
