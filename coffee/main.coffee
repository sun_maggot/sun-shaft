Basic = require './Basic.coffee'
Entity = require './Entity.coffee'
Game = require './Game.coffee'
resources = require './resources.coffee'

$ = require 'jquery'
pixi = require 'pixi.js'
images = require '../images/manifest.json'

testLight = (game) ->

	scene = game.scene
	scene.background = 0xA25337
	Player = require './Player.coffee'
	Decoration = require './Decoration.coffee'
	CrepMap = require './CrepMap.coffee'
	Sun = require './Sun.coffee'

	player = new Player game
	player.z = 1
	player.y = 170
	player.sprite.tint = 0xB0B0B0
	scene.addEntity player

	main_deco = new Decoration game
	main_deco.texture = 'images/shop_street1_main.png'
	main_deco.z = 0
	main_deco.y = -55
	scene.cameraBounds.width = main_deco.sprite.width
	console.log main_deco.sprite.width
	scene.addEntity main_deco
	 
	interior_deco = new Decoration game
	interior_deco.texture = 'images/shop_street1_interior.png'
	interior_deco.z = -1
	interior_deco.y = 90
	interior_deco.parallax.x = 0.8
	scene.addEntity interior_deco

	scene.preDraw.do (dt) =>
		scene.camera.x = player.getDrawX(scene.camera) - game.width / 2
		return

	crepMap = new CrepMap game
	crepMap.blockers.push interior_deco
	crepMap.blockers.push main_deco
	crepMap.blockers.push player
	scene.addEntity crepMap

	sun = new Sun game
	sun.parallax.x = 0
	sun.x = 150
	sun.y = 30 
	sun.light.tint = 0xFFA673
	# sun.light.tint = 0xFFFFFF
	crepMap.source = sun
	scene.addEntity sun

	blurFilter = new pixi.filters.BlurFilter

$(document).ready () ->
	resources.finished.then (res) =>
		game = new Game res

		testLight game

	.done()

	console.log 'done'
