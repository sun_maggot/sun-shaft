
pixi = require 'pixi.js'
Entity = require './Entity.coffee'

module.exports = class Sun extends Entity
	@TYPE('Sun', this)
	constructor: (game) ->
		super(game)
		@z = -10
		# @displayObject.renderable = false
		
		@centerSprite = new pixi.Sprite @game.newTexture 'images/sun_center.png'
		@centerSprite.anchor.x = 0.5
		@centerSprite.anchor.y = 0.5
		@centerSprite.scale.x = 1.5
		@centerSprite.scale.y = 1.5
		@displayObject.addChild @centerSprite

		@light = new pixi.Sprite @game.newTexture 'images/sun.png'
		@light.anchor.x = 0.5
		@light.anchor.y = 0.5

	getDisplayObject: () -> new pixi.Container
