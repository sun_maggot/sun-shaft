Entity = require './Entity.coffee'
pixi = require 'pixi.js'

module.exports = class Decoration extends Entity

	@TYPE('Decoration', this)
	constructor: (game) ->
		super(game)
		@textureName_ = ""
		@sprite = null

		@addTextureAttr 'texture'

	setSpriteFromTextureName: (textureName) ->
		console.log 'setting texture to', textureName
		if textureName == @textureName_
			return

		if @sprite
			@displayObject.removeChild @sprite
			@sprite = null

		@textureName_ = textureName
		if @textureName_ == ""
			return
		@sprite = new pixi.Sprite @game.newTexture(@textureName_)
		@displayObject.addChild @sprite

Object.defineProperty Decoration.prototype, 'texture',
	get: () -> @textureName_
	set: (val) -> @setSpriteFromTextureName val
