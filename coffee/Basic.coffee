q = require 'q'

module.exports = class Basic
	@ATTR_TYPES =
		STRING: 0
		NUMBER: 1
		RANGE: 2
		COLOR: 3
		POINT: 4
		BOOLEAN: 5
		POINT_RANGE: 6
		TEXTURE: 7

	@CONSTRUCTORS = {}

	@TYPE: (typeName, constr) ->
		@TYPE_NAME = typeName
		@CONSTRUCTORS[typeName] = constr

	@TYPE('Basic', this)
	constructor: (@game) ->
		@attributes = {}
		@typeName = @constructor.TYPE_NAME
		@scripts = []
		@q = q

	update: (dt) ->
		@updateScripts(dt)

	draw: (dt) ->

	getAttrNames: () ->
		names = []
		for k, v of @attributes
			names.push k
		return names

	addAttr: (name, type, options) ->
		@attributes[name] =
			type: type
			options: options

	getAttrType: (name) -> @attributes[name].type
	getAttrOptions: (name) -> @attributes[name].options
	getAttrValue: (name) -> this[name]
	setAttrValue: (name, value) -> this[name] = value

	addPointAttr: (name, options) -> @addAttr name, @constructor.ATTR_TYPES.POINT, options
	addPointRangeAttr: (name, options) -> @addAttr name, @constructor.ATTR_TYPES.POINT_RANGE, options
	addStringAttr: (name, options) -> @addAttr name, @constructor.ATTR_TYPES.STRING, options
	addNumberAttr: (name, options) -> @addAttr name, @constructor.ATTR_TYPES.NUMBER, options
	addRangeAttr: (name, options) -> @addAttr name, @constructor.ATTR_TYPES.RANGE, options
	addColorAttr: (name, options) -> @addAttr name, @constructor.ATTR_TYPES.COLOR, options
	addBooleanAttr: (name, options) -> @addAttr name, @constructor.ATTR_TYPES.BOOLEAN, options
	addTextureAttr: (name, options) -> @addAttr name, @constructor.ATTR_TYPES.TEXTURE, options

	serialize: () ->
		obj = {}
		attrNames = @getAttrNames()
		for name in attrNames
			obj[name] = @getAttrValue(name)
		return obj

	deserialize: (obj) ->
		for name, value of obj
			@setAttrValue name, value

	createScript: (func) ->
		update: func
		deferred: q.defer()

	addScript: (script) ->
		@scripts.push script

	updateScripts: (dt) ->
		newScripts = []
		for s in @scripts
			result = s.update(dt)
			if result != undefined
				s.deferred.resolve(result)
			else
				newScripts.push s
		@scripts = newScripts

	do: (func) ->
		script = @createScript func
		@addScript script
		return script.deferred.promise