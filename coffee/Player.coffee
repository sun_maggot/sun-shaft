Entity = require './Entity.coffee'
Sprite = require './components/Sprite.coffee'
pixi = require 'pixi.js'

SPEED = 100

module.exports = class Player extends Entity
	@TYPE('Player', this)
	constructor: (game) ->
		super(game)
		@sprite = new Sprite @game.newTexture('images/player.png'), 8, 4
		@sprite.anchor.x = 0.5
		@sprite.anchor.y = 1
		@displayObject.addChild @sprite

		@sprite.addAnim 'idle', [0], 11, false
		@sprite.addAnim 'walk', [8, 9, 10, 11, 12, 13, 14, 15], 11, true

		@do (dt) =>
			if @game.kb.isDown('left') and not @game.kb.isDown('right')
				@walk()
				@faceLeft()
				@x -= SPEED * dt
			else if not @game.kb.isDown('left') and @game.kb.isDown('right')
				@walk()
				@faceRight()
				@x += SPEED * dt
			else
				@idle()
			return

		@x = 100
		@y = 100

	update: (dt) ->
		super(dt)
		@sprite.update(dt)
	walk: () -> @sprite.play 'walk'
	idle: () -> @sprite.play 'idle'
	faceLeft: () -> @sprite.scale.x = -1
	faceRight: () -> @sprite.scale.x = 1
