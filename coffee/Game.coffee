Scene = require './Scene.coffee'
pixi = require 'pixi.js'
$ = require 'jquery'
Keyboard = require './components/Keyboard.coffee'
# Designer = require './Designer.coffee'
Basic = require './Basic.coffee'


module.exports = class Game

	constructor: (resources) ->

		@width = 240
		@height = 200
		@scale = 4

		@baseTextures = resources.baseTextures
		@renderer = new pixi.WebGLRenderer(@width * @scale, @height * @scale)
		@root = new pixi.Container()
		@finalTexture = new pixi.RenderTexture @renderer, @width, @height, pixi.SCALE_MODES.NEAREST
		@finalTextureSprite = new pixi.Sprite @finalTexture
		@finalTextureSprite.scale.x = @scale
		@finalTextureSprite.scale.y = @scale

		@newScene()
		@kb = new Keyboard()

		@postDraw = new Basic this

		$('#game-container').append @renderer.view
		# jDesignerContainer = $('#editor-container')
		# designer = new Designer this

		last = Date.now()
		step = () =>
			now = Date.now()
			dt = (now - last) / 1000
			last = now

			if not @paused
				@scene.update(dt)
			@scene.draw(dt)

			@postDraw.update dt

			@finalTexture.render @root
			@renderer.render @finalTextureSprite
			@kb.reset()

			# designer.update(dt)
			window.requestAnimationFrame step

		step()

	getBaseTextureNames: () ->
		names = []
		for k, v in @baseTextures
			names.push k
		return names

	newScene: () ->
		@changeScene new Scene(this)

	changeScene: (scene) ->
		if @scene
			@root.removeChild @scene.root
		@scene = scene
		@root.addChild @scene.root

	newTexture: (name) -> new pixi.Texture(@baseTextures[name])


