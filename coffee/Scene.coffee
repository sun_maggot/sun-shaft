Basic = require './Basic.coffee'
pixi = require 'pixi.js'

module.exports = class Scene extends Basic
	@TYPE("Scene", this)

	constructor: (game) ->
		super(game)
		@root = new pixi.Container()
		@backgroundGraphics = new pixi.Graphics()
		@root.addChild @backgroundGraphics
		@container = new pixi.Container()
		@root.addChild @container
		@entities = []
		@name = ""
		@camera = { x: 0, y: 0 }

		@preEntity = new Basic(@game)
		@postEntity = new Basic(@game)
		@preDraw = new Basic(@game)
		@postDraw = new Basic(@game)

		@backgroundGraphics.clear()
		@backgroundGraphics.beginFill(0xFFFFFF, 1)
		@backgroundGraphics.drawRect(0, 0, 1, 1)
		@backgroundGraphics.endFill()
		@backgroundGraphics.tint = 0x000000

		@cameraBounds =
			x: 0
			y: 0
			width: @game.width
			height: @game.height

		Object.defineProperty this, 'background',
			get: () -> @backgroundGraphics.tint
			set: (val) -> 
				@backgroundGraphics.tint = val

		@addStringAttr 'region'
		@addColorAttr 'background'

	update: (dt) ->
		@preEntity.update(dt)

		for e in @entities
			e.update(dt)

		@postEntity.update(dt)

	draw: (dt) ->
		@preDraw.update(dt)

		for e in @entities
			e.draw(dt)
		for e, i in @entities
			e.updateDisplayZ()
			e.index__ = i
			e.displayObject.index__ = i

		#sor the objects by z index
		sortFunc = (a, b) ->
			if a.z != b.z
				return a.z > b.z
			else
				return a.index__ > b.index__
		@container.children.sort sortFunc
		@entities.sort sortFunc

		if @game.width <= @cameraBounds.width
			if @camera.x < @cameraBounds.x
				@camera.x = @cameraBounds.x
			else if @camera.x > @cameraBounds.x + @cameraBounds.width - @game.width
				@camera.x = @cameraBounds.x + @cameraBounds.width - @game.width
		else
			@camera.x = @cameraBounds.x + (@cameraBounds.width - @game.width) / 2

		@container.x = -@camera.x
		@container.y = -@camera.y

		for e in @entities
			e.updateDisplayPosition(@camera)

		@backgroundGraphics.scale.x = @game.width
		@backgroundGraphics.scale.y = @game.height

		@postDraw.update(dt)

	addEntity: (entity) ->
		@entities.push entity
		@container.addChild entity.displayObject
		entity.id = @findFirstfreeID(entity.typeName)
		entity.scene = this

	removeEntity: (entity) ->
		index = @entities.indexOf entity
		if index == -1
			return
		@entities.splice index, 1
		@container.removeChild entity.displayObject
		entity.scene = null

	hasEntityWithID: (id) -> @get(id) != null

	serialize: () ->
		obj =
			attr: super()
			entities: []

		for e in @entities
			obj.entities.push
				typeName: e.typeName
				attr: e.serialize()

		return obj

	deserialize: (obj) ->
		super(obj.attr)
		for eobj in obj.entities
			entity = new @constructor.CONSTRUCTORS[eobj.typeName](@game)
			entity.deserialize eobj.attr
			@entities.push entity
			@container.addChild entity.displayObject

	findFirstfreeID: (prefix) ->
		index = 0
		newID = prefix + index
		while @hasEntityWithID(newID)
			newID = prefix + (++index)
		return newID

	get: (id) ->
		for e in @entities
			if e.id == id
				return e
		return null
