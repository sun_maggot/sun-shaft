
pixi = require 'pixi.js'
Basic = require './Basic.coffee'

module.exports = class Entity extends Basic

	@TYPE('Entity', this)

	getDisplayObject: () -> new pixi.Container()

	constructor: (game) ->
		super(game)
		@displayObject = @getDisplayObject()
		@x = 0
		@y = 0
		@z = 0
		@parallax = { x: 1, y: 1 }
		@id = null
		@scene = null

		@addNumberAttr 'x'
		@addNumberAttr 'y'
		@addNumberAttr 'z'
		@addPointRangeAttr 'parallax', { max: 2, min: 0, step: 0.1}
		@addStringAttr 'id'
		@addRangeAttr 'alpha', { max: 1, min: 0, step: 0.05 }
		@addBooleanAttr 'visible'

		@showInScene = true

	update: (dt) ->
		super(dt)


	updateDisplayPosition: (camera) ->
		@displayObject.x = @getDrawX(camera)
		@displayObject.y = @getDrawY(camera)

	updateDisplayZ: () ->
		@displayObject.z = @z


	getDrawX: (camera) -> Math.floor(@x + camera.x * (1 - @parallax.x))
	getDrawY: (camera) -> Math.floor(@y + camera.y * (1 - @parallax.y))


Object.defineProperty Entity.prototype, 'scale',
	get: () -> @displayObject.scale
Object.defineProperty Entity.prototype, 'rotation',
	get: () -> @displayObject.rotation
	set: (val) -> @displayObject.rotation = val
Object.defineProperty Entity.prototype, 'visible',
	get: () -> @displayObject.visible
	set: (val) -> @displayObject.visible = val
Object.defineProperty Entity.prototype, 'alpha',
	get: () -> @displayObject.alpha
	set: (val) -> @displayObject.alpha = val
