pixi = require 'pixi.js'
q = require 'q'

module.exports = class Sprite extends pixi.Sprite
	constructor: (texture, num_cols = 1, num_rows = 1) ->
		super(texture)

		@num_cols = num_cols
		@num_rows = num_rows

		@frame_rect = new PIXI.Rectangle 0, 0, texture.width / num_cols, texture.height / num_rows

		@max_frame = num_rows * num_cols
		@frame_index_ = 0
		@setFrameIndex 0

		@animations = {}
		@cur_anim = null
		@cur_anim_defer = null
		

	setFrameIndex: (frame) ->
		frame = Math.floor(frame)
		frame = Math.max(0, frame)
		frame = Math.min(frame, @max_frame)

		@frame_index_ = frame
		@frame_rect.x = Math.floor(frame % @num_cols) * @frame_rect.width
		@frame_rect.y = Math.floor(frame / @num_cols) * @frame_rect.height

		@texture.frame = @frame_rect
		return

	getFrameIndex: () -> @frame_index_


	getMaxFrame: () -> @max_frame
	getFrameWidth: () -> @frame_rect.width
	getFrameHeight: () -> @frame_rect.height

	addAnim: (name, frames, fps, looped) ->
		@animations[name] = 
			frames: frames
			fps: fps
			looped: looped

	updateAnim: (dt) ->

		if @cur_anim
			anim = @cur_anim
			@remaining -= dt
			if @remaining <= 0
				@cur_index++
				@remaining = 1 / @cur_anim.fps

				if @cur_index >= @cur_anim.frames.length
					if @cur_anim.looped
						@cur_index = 0
					else
						@cur_anim = null
						@cur_anim_defer.resolve()
						@cur_anim_defer = null
						return

			@frameIndex = @cur_anim.frames[@cur_index]

	play: (name, force) -> 
		
		if not force and @cur_anim == @animations[name]
			return @cur_anim_defer

		@cur_anim = @animations[name]
		@cur_anim_defer = q.defer()
		@cur_index = 0
		@remaining = 1 / @cur_anim.fps

		return @cur_anim_defer.promise

	update: (dt) ->
		@updateAnim dt

Object.defineProperty Sprite.prototype, 'frameIndex',
		get: () -> @getFrameIndex()
		set: (v) -> @setFrameIndex(v)

Object.defineProperty Sprite.prototype, 'frame_width',
		get: () -> @frame_rect.width
	
Object.defineProperty Sprite.prototype, 'frame_height',
		get: () -> @frame_rect.height
	
