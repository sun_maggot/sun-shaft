Mousetrap = require('mousetrap')

keys = [ 
	'left', 
	'right', 
	'up', 
	'down', 
	'w', 
	'a', 
	's', 
	'd', 
	'x',
	'p',
	',',
	'l',
	'enter',
	'escape',
	'space',
]

module.exports = class Keyboard
	constructor: () ->
		# @listener = new Keypress.Listener
		@state = {}
		for k in keys
			@state[k] =
				justDown : false
				justUp : false
				down : false

			((k) =>
				Mousetrap.bind(k, (e) =>
						@state[k].justDown = true
						@state[k].down = true
					,
					'keydown')
				Mousetrap.bind(k, (e) =>
						@state[k].justUp = true
						@state[k].down = false
					,
					'keyup')
			)(k)

	reset: () ->
		for k,v of @state
			v.justDown = false
			v.justUp = false

	isDown: (key) ->
		return @state[key] and @state[key].down

	isJustDown: (key) ->
		return @state[key] and @state[key].justDown

	isJustUp: (key) ->
		return @state[key] and @state[key].justUp
