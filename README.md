# Sun Shaft #
![Alt text](https://media.giphy.com/media/3oEjI8jFKUCxPASnSw/giphy.gif)

Left and right arrow to walk around.

Don't be alarmed by the many .coffee files! This demo was extracted from a bigger project.

The shader code for the effect is in CrepMap.coffee. "Crep" stands for "crepuscular".

# Run #

Start a simple HTTP server. The easiest way to do so is:
```
python -m SimpleHTTPServer 8080
```
Then go to http://localhost:8080/index.html

# Build #
Install node.js, then:

```
npm install
node build.js
```