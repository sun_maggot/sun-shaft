var browserify = require('browserify');
var fs = require("fs");
var outputFile = fs.createWriteStream('main.js');
var b = browserify({
	transform : [ "coffeeify" ]
});
b.add('./coffee/main.coffee');
b.bundle().pipe(outputFile);
